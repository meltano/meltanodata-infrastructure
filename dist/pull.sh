#!/bin/bash

date > LATEST
echo "--- git fetch origin ---" >> LATEST
git fetch origin >> LATEST
echo "--- git checkout --force master ---" >> LATEST
git checkout --force master >> LATEST
echo "--- git reset --hard origin/master ---" >> LATEST
git reset --hard origin/master >> LATEST
