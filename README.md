# Meltano Infrastructure

## Playbooks

See the documentation at <https://meltano.com/handbook/engineering/meltanodata-guide/controller-node.html#playbooks>

### Executing a playbook

See the documentation at <https://meltano.com/handbook/engineering/meltanodata-guide/controller-node.html#using-ansible>

## How to add sensitive data

We use [Ansible Vault](https://docs.ansible.com/ansible/latest/user_guide/vault.html) to add sensitive variables, files, etc… to the infrastructure 

First, get the `Ansible Vault` password available in our 1Password Vault.

You can either store it in `.vault-password` for future use, or send it ad-hoc with `--ask-vault-pass`.

> If you think this password has been compromised, contact your manager immediately to generate a new key and re-encrypt all the files.

Then, use the `ansible-vault` command to encrypt the file.

```bash
ansible-vault [--ask-vault-pass] file/to/encrypt --output file/is/encrypted.vlt
```

We use the `.vlt` extension to mark a file as having encrypted content: this is handy for re-keying purpose and to filter out the files from linting for instance.

Then, make sure to delete the source file.
