---
- hosts: '*.meltanodata.com'
  gather_facts: yes
  vars:
    home_dir: /var/meltano
    scripts_dir: "{{ home_dir }}/scripts"
    project_dir: "{{ home_dir }}/project"
    env_dir: /etc/meltano/environment.d
  tasks:
    - include_vars:
        name: oauth
        file: vars/meltano/oauth.yml.vlt

    - include_vars:
        name: smtp
        file: vars/meltano/smtp.yml.vlt

    - name: Create the `meltano` user
      user:
        name: meltano
        shell: /bin/bash
        system: true
        home: /var/meltano
        create_home: no
      tags:
        - bootstrap

    - name: /etc/meltano/environment.d
      file:
        path: "{{ env_dir }}"
        owner: meltano
        group: meltano
        mode: "06700"
        state: directory

    # this file should only host the secrets environment files
    # as it requires a full restart of the `meltano` service
    - name: /etc/meltano/environment.d/secrets
      template:
        src: templates/meltano/secrets.j2
        dest: "{{ env_dir }}/secrets"
        owner: meltano
        group: meltano
        mode: "0600"
      notify:
        - restart meltano

    - name: /var/meltano/scripts
      file:
        path: "{{ scripts_dir }}"
        owner: meltano
        group: meltano
        mode: "06700"
        state: directory

    - name: /var/meltano/scripts/meltano_reload.sh
      copy:
        src: files/meltano/meltano_reload.sh
        dest: "{{ scripts_dir }}/meltano_reload.sh"
        owner: meltano
        group: meltano
        mode: "0700"

    - name: meltano.service
      template:
        src: systemd/meltano.service
        dest: /etc/systemd/system/meltano.service
      notify:
        - restart meltano

    # this file should host feature flags for the
    # application as it can be hot-loaded
    #
    # uses the `meltano.env` variable
    - name: .env
      template:
        src: templates/meltano/dotenv.j2
        dest: "{{ project_dir }}/.env"
        owner: meltano
        group: meltano
        mode: "0660"
      notify:
        - reload meltano

    - name: Generate the application secrets
      become_user: meltano
      become: yes
      shell: |
        source {{ home_dir }}/.venv/bin/activate
        meltano ui setup '{{ meltano.server_name }}'
      args:
        chdir: "{{ project_dir }}"
        executable: /bin/bash
        creates: "{{ project_dir }}/ui.cfg"
      notify:
        - reload meltano

    - name: Ensure file-system ACLs
      file:
        dest: "{{ project_dir }}/.meltano"
        mode: '2720'
        owner: meltano
        group: meltano
        state: directory

  handlers:
    - name: restart meltano
      systemd:
        name: meltano
        daemon_reload: yes
        state: restarted
      when: meltano.enabled

    - name: reload meltano
      systemd:
        name: meltano
        daemon_reload: yes
        state: reloaded
      when: meltano.enabled
