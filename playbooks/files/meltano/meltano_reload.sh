#!/bin/bash

# fail on any error
set -e

PID_PATH=.meltano/run/gunicorn.pid 

if [[ -f $PID_PATH ]];
then
  /usr/bin/pkill -HUP --pidfile $PID_PATH
else
  echo Cannot reload, $PID_PATH is missing.
  # this is important so that systemd thinks everything when smoothly
  exit 0
fi

