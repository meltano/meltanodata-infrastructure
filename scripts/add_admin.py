#!/usr/bin/env python3

"""
Script to add a Meltano UI admin record with a (randomly generated) password to the playbooks/vars/meltano/admins.yml.vlt vault.

To be run from the infrastructure project root.

Usage:
    ./scripts/add_admin.py subdomain username [password]
"""

import sys
from tools import add_admin

def main():
  subdomain = sys.argv[1]
  username = sys.argv[2]

  try:
    password = sys.argv[3]
  except IndexError:
    password = None

  add_admin(subdomain, username, password)

if __name__ == '__main__':
    main()