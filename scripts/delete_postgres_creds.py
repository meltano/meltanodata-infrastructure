#!/usr/bin/env python3

"""
Script to delete Postgres connection details from the playbooks/vars/meltano/postgres_creds.yml.vlt vault.

To be run from the infrastructure project root.

Usage:
    ./scripts/delete_postgres_creds.py subdomain
"""

import sys
from tools import delete_postgres_creds

def main():
  subdomain = sys.argv[1]

  delete_postgres_creds(subdomain)

if __name__ == '__main__':
    main()