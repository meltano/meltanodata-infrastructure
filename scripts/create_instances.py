#!/usr/bin/env python3

"""
Script to create MeltanoData.com instances (droplet, domain record, and database) on DigitalOcean.

Environment variables:
    DO_API_TOKEN: API token for the main 'Meltano' DO account
    DO_DATABASE_API_TOKENS: Comma-separated API tokens for the 'MeltanoData DBs' DO accounts

Usage:
    ./create_instances.py [subdomain ...]

Example:
    ./create_instances.py user1 user2
"""

import os
import sys
from tools.instance_creator import InstanceCreater


def main():
    if "DO_API_TOKEN" in os.environ:
        api_token = os.getenv("DO_API_TOKEN")
    else:
        msg = "API token for the main 'Meltano' DO account must be provided in DO_API_TOKEN environment variable\n"
        sys.stderr.write(msg)
        sys.exit(-1)

    if "DO_DATABASE_API_TOKENS" in os.environ:
        tokens_string = os.getenv("DO_DATABASE_API_TOKENS")
        database_api_tokens = [c.strip() for c in tokens_string.split(",")]
    else:
        msg = "API tokens for the 'MeltanoData DBs' DO accounts must be provided in comma-separated DO_DATABASE_API_TOKENS environment variable\n"
        sys.stderr.write(msg)
        sys.exit(-1)

    if len(sys.argv) == 1:
        msg = 'MeltanoData.com subdomain must be provided in command line arguments\n'
        sys.stderr.write(msg)
        sys.exit(-1)

    creater = InstanceCreater(api_token, database_api_tokens)

    for subdomain in sys.argv[1:]:
        instance_data = creater.create_instance(subdomain)

        print(f"Instance URL: {instance_data['url']}")
        print()

        print("Database connection details:")
        print()
        connection = instance_data['database']['connection']
        print(f"URI: {connection['uri']}")
        print(f"PG_USERNAME={connection['user']}")
        print(f"PG_PASSWORD={connection['password']}")
        print(f"PG_ADDRESS={connection['host']}")
        print(f"PG_PORT={connection['port']}")
        print(f"PG_DATABASE={connection['database']}")
        print()

if __name__ == '__main__':
    main()