#!/usr/bin/env python3

#
# This script can be used to extract usage information
# from remote meltano instances
#
# This currently simply wraps an `ansible` invocation,
# but it could leverage a custom callback plugin or the
# ansible programatic API in the future
#

import os
import logging
import io
import shutil
import subprocess
import sys
import json
import csv
import tempfile
from enum import IntEnum


class Task(IntEnum):
    GATHER_FACTS = 0
    EXTRACT_INSTANCE_CREATED_AT = 2
    EXTRACT_LOGIN_DATA = 3
    EXTRACT_EXTRACTORS = 4
    EXTRACT_PIPELINES = 5


env = {
    'ANSIBLE_LOAD_CALLBACK_PLUGINS': 'True',
    'ANSIBLE_STDOUT_CALLBACK': 'json',
    **os.environ,
}

def ansible_run(*args) -> dict:
    result = subprocess.run(["ansible-playbook", *args],
                            stdout=subprocess.PIPE,
                            env=env)

    return json.loads(result.stdout)


# there is only 1 play in that playbook
data = ansible_run('playbooks/usage-data.yml')
tasks = data["plays"][0]["tasks"]

id_facts = {
    host: task.get("ansible_facts", {})
    for host, task in tasks[Task.GATHER_FACTS]["hosts"].items()
}

id_instance_created_at = {
    host: task.get("msg", None)
    for host, task in tasks[Task.EXTRACT_INSTANCE_CREATED_AT]["hosts"].items()
}

id_extractors = {
    host: task["stdout"]
    for host, task in tasks[Task.EXTRACT_EXTRACTORS]["hosts"].items()
}

id_pipelines = {
    host: task["stdout"]
    for host, task in tasks[Task.EXTRACT_PIPELINES]["hosts"].items()
}

usage_data = []
# let's construct the usage data
for host, task in tasks[Task.EXTRACT_LOGIN_DATA]["hosts"].items():
    try:
        reader = csv.reader(task["stdout_lines"][1:])

        for row in reader:
            usage_data.append({
                "instance": id_facts[host].get("ansible_fqdn", "UNKNOWN"),
                "user": row[0],
                "last_login_at": row[1],
                "login_count": row[2],
                "last_activity_at": row[3],
                "instance_created_at": id_instance_created_at[host],
                "extractors": id_extractors[host],
                "pipelines": id_pipelines[host],
            })
    except Exception as err:
        logging.error(str(err))

try:
    out = tempfile.NamedTemporaryFile(delete=False, mode="w")
    # output in CSV
    writer = csv.DictWriter(out, fieldnames=usage_data[0].keys())
    writer.writeheader()

    for entry in usage_data:
        writer.writerow(entry)
finally:
    out.close()

# create the output file
shutil.move(out.name, sys.argv[1])
