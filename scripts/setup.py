#!/usr/bin/env python3

"""
Script to set up a MeltanoData instance.

Environment variables:
    DO_API_TOKEN: API token for the main 'Meltano' DO account
    ZAPIER_WEBHOOK_URL: Webhook URL for Zapier 'New Instance Webhook' zap
    MELTANO_ADMIN_PASSWORD: Password for the 'meltano' admin user

Usage:
    ./setup.py "Full Name" email@example.com subdomain
"""

import os
import sys
import subprocess
import re
import tools
import requests
from tools.instance_creator import InstanceCreater

def run_cmd(*args, su=None, **kwargs):
    if su:
        args = ["sudo", "-H", "-E", "-u", su, *args]

    print()
    print(f"--- {str(args)[1:-1]} ---")
    res = subprocess.run(args, check=True, **kwargs)
    print(f"---")

    res.check_returncode()

    return res

def run_func(func, *args):
    print()
    print(f"--- {func.__name__}({str(args)[1:-1]}) ---")
    res = func(*args)
    print(f"---")

    return res

DOMAIN_NAME = 'meltanodata.com'

try:
    api_token = os.environ["DO_API_TOKEN"]
except KeyError:
    raise Exception("API token for the main 'Meltano' DO account must be provided in DO_API_TOKEN environment variable")

creater = InstanceCreater(api_token)

try:
    webhook_url = os.environ["ZAPIER_WEBHOOK_URL"]
except KeyError:
    raise Exception("Webhook URL for Zapier 'New Instance Webhook' zap must be provided in ZAPIER_WEBHOOK_URL environment variable")

def trigger_webhook(data):
    response = requests.post(webhook_url, json=data)
    response.raise_for_status()
    print(f"Triggered Zapier webhook to email user and post to Slack")
    return response

try:
    meltano_admin_password = os.environ["MELTANO_ADMIN_PASSWORD"]
except KeyError:
    raise Exception("Password for the 'meltano' admin user must be provided in MELTANO_ADMIN_PASSWORD environment variable")

try:
    full_name = sys.argv[1]
except IndexError:
    raise Exception("Full name must be provided in first command line argument")

try:
    email = sys.argv[2]
except IndexError:
    raise Exception("Email must be provided in second command line argument")

if not re.fullmatch(r"[^@ ]+@[^@ ]+\.[^@ ]+", email):
    raise Exception("Email is not formatted correctly")

try:
    subdomain = sys.argv[3]
except IndexError:
    raise Exception("Subdomain must be provided in third command line argument")

def is_subdomain_in_use(subdomain):
    res = run_cmd("ansible", "--list-hosts", f"{subdomain}.{DOMAIN_NAME}", su="ansible", stdout=subprocess.PIPE, universal_newlines=True)
    output = res.stdout

    number_of_hosts_match = re.search(r"\(([0-9]+)\)", output)
    if not number_of_hosts_match:
        raise Exception(f"Could not match number in hosts in `ansible --list-hosts` output: {output}")

    number_of_hosts = int(number_of_hosts_match[1])
    return number_of_hosts > 0


def sanitize_subdomain(subdomain):
    # Only lowercase characters are allowed
    subdomain = subdomain.lower()
    # Remove `.meltanodata.com` suffix if it was included by accident
    subdomain = re.sub(rf".{re.escape(DOMAIN_NAME)}$", "", subdomain)
    # Remove characters other than a-z, 0-9 and -
    subdomain = re.sub(r"[^a-z0-9-]", "", subdomain)
    # Remove trailing and ending dashes
    subdomain = re.sub(r"^-*(.*?)-*$", "\\1", subdomain)

    original_subdomain = subdomain
    counter = 1

    while is_subdomain_in_use(subdomain):
        counter += 1
        subdomain = f"{original_subdomain}-{counter}"

    return subdomain

print(f"Sanitizing subdomain '{subdomain}' and ensuring it is not already in use...")

subdomain = sanitize_subdomain(subdomain)
hostname = f"{subdomain}.{DOMAIN_NAME}"

print()
print(f"Setting up instance {hostname}...")

run_cmd("pip3", "install", "-r", "requirements.txt")

run_cmd("dist/pull.sh")

database = run_func(creater.create_database, subdomain)

droplet = run_func(creater.create_droplet, subdomain)

run_func(creater.create_domain_record, subdomain, droplet)

run_func(creater.update_database_firewall, database, droplet)

admins = []
any_admin_is_new = False

user_admin, is_new = run_func(tools.add_admin, subdomain, subdomain)
admins.append(user_admin)
if is_new: any_admin_is_new = True

meltano_admin, is_new = run_func(tools.add_admin, subdomain, 'meltano', meltano_admin_password)
admins.append(meltano_admin)
if is_new: any_admin_is_new = True

if any_admin_is_new:
    run_cmd("git", "commit", "-am", "Add admin users for new instance")
    run_cmd("git", "push", su="ansible")

creds, is_new = run_func(tools.set_postgres_creds, subdomain, database['connection']['uri'])

if is_new:
    run_cmd("git", "commit", "-am", "Add Postgres connection details for new instance")
    run_cmd("git", "push", su="ansible")

while True:
    try:
        run_cmd("ansible", hostname, "-m", "ping", su="ansible")
        break
    except subprocess.CalledProcessError as err:
        print(err)
        print("Failed to connect, retrying.")

run_cmd("ansible-playbook", "playbooks/setup.yml", "--limit", hostname, su="ansible")

run_cmd("ansible", hostname, "-a", "systemctl status meltano", su="ansible")

run_func(creater.wait_for_database_to_be_online, database)

run_func(
    trigger_webhook,
    {
        "name": full_name,
        "email": email,
        "url": f"https://{hostname}",
        "user_admin": user_admin
    }
)

print()
print(f"Successfully set up instance at https://{hostname}!")

print()
print("Admins:")
for admin in admins:
    print(f"Username: {admin['username']}")
    print(f"Password: {admin['password']}")
    print()