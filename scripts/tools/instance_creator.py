import time
import random
import digitalocean

DOMAIN_NAME = 'meltanodata.com'

class InstanceCreaterError(Exception):
    pass

class InstanceCreater:
    def __init__(self, api_token, database_api_tokens=list()):
        self.manager = digitalocean.Manager(token=api_token)
        self.database_managers = [
            self.manager,
            *(digitalocean.Manager(token=api_token) for api_token in database_api_tokens)
        ]

    def create_instance(self, subdomain):
        droplet = self.create_droplet(subdomain)
        print()

        domain_record = self.create_domain_record(subdomain, droplet)
        print()

        database = self.set_up_database(subdomain, droplet)
        print()

        return {
            "url": f"https://{subdomain}.{DOMAIN_NAME}",
            "droplet": droplet,
            "domain_record": domain_record,
            "database": database
        }

    def create_droplet(self, subdomain):
        print(f"Creating droplet for subdomain '{subdomain}'...")

        ssh_key_ids = [key.id for key in self.manager.get_all_sshkeys()]
        snapshot = [s for s in self.manager.get_all_snapshots() if s.name.startswith('meltano-')][-1]

        droplet = digitalocean.Droplet(
            token=self.manager.token,
            name=f"{subdomain}.{DOMAIN_NAME}",
            region='nyc3',
            image=snapshot.id,
            size_slug='s-2vcpu-2gb',
            ssh_keys=ssh_key_ids,
            tags=['production']
        )
        droplet.create()
        print(f"Successfully created droplet with ID '{droplet.id}'!")

        actions = droplet.get_actions()
        for action in actions:
            print(f"Waiting for '{action.type}' action to complete...")
            if action.wait():
                print(f"Action '{action.type}' completed successfully!")
            else:
                raise InstanceCreaterError(f"Action '{action.type}' completed with status '{action.status}'")

        droplet.load()

        print(f"Droplet was assigned IP '{droplet.ip_address}'.")

        return droplet

    def create_domain_record(self, subdomain, droplet):
        print(f"Creating domain record for subdomain '{subdomain}'...")

        domain = self.manager.get_domain(DOMAIN_NAME)
        record_data =  domain.create_new_domain_record(
            type='A',
            name=subdomain,
            data=droplet.ip_address,
            ttl=3600
        )
        record = record_data['domain_record']

        print(f"Successfully created record with ID '{record['id']}'!")

        return record


    def set_up_database(self, subdomain, droplet):
        database = self.create_database(subdomain)

        self.update_database_firewall(database, droplet)

        database = self.wait_for_database_to_be_online(database)

        return database

    def create_database(self, subdomain):
        print(f"Creating database for subdomain '{subdomain}'...")

        database_data = self.manager.get_data(
            "databases",
            type='POST',
            params={
                "name": f"db-postgresql-nyc3-{''.join(random.sample('0123456789', 5))}-{subdomain}",
                "engine": "pg",
                "size": "db-s-1vcpu-1gb",
                "region": 'nyc3',
                "num_nodes": 1,
                "tags": ['production']
            }
        )

        if not database_data:
            raise InstanceCreaterError("Database could not be created")

        database = database_data["database"]

        print(f"Successfully created database with ID '{database['id']}'!")

        return database
    
    def wait_for_database_to_be_online(self, database):
        def reload_database():
            database_data = self.manager.get_data("databases/%s" % database["id"])
            return database_data["database"]

        database = reload_database()

        if database['status'] == 'online':
            return database

        print(f"Waiting for database status to transition from '{database['status']}' to 'online'...")

        while database['status'] != 'online':
            print("Checking status...")
            database = reload_database()
            time.sleep(10)

        print(f"Database is now online!")

        return database
    
    def update_database_firewall(self, database, droplet):
        print("Updating firewall rules...")
        firewall_updated = self.manager.get_data(
            "databases/%s/firewall" % database["id"],
            type='PUT',
            params={
                "rules": [
                    {
                        "type": "droplet",
                        "value": str(droplet.id)
                    }
                ]
            }
        )

        if not firewall_updated:
            raise InstanceCreaterError(f"Firewall rules could not be updated")

        print("Successfully updated firewall rules!")
