import string
import random
from .vault import get_vault

DOMAIN_NAME = 'meltanodata.com'
VAULT_PATH = 'playbooks/vars/meltano/admins.yml.vlt'

def delete_admins(subdomain):
  hostname = f"{subdomain}.{DOMAIN_NAME}"

  vault = get_vault()
  admins = vault.load(open(VAULT_PATH, 'r').read())

  try:
    del admins[hostname]
  except KeyError:
    print(f"No admins found to delete for https://{hostname}")

    return False

  vault.dump(admins, open(VAULT_PATH, 'wb'))

  print(f"Deleted admin users for https://{hostname}")

  return True
