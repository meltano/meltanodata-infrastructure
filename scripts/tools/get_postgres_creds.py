from .vault import get_vault

VAULT_PATH = 'playbooks/vars/meltano/postgres_creds.yml.vlt'

def get_postgres_creds(hostname):
  vault = get_vault()
  postgres_creds = vault.load(open(VAULT_PATH, 'r').read())

  try:
    return postgres_creds[hostname]
  except KeyError:
    return None