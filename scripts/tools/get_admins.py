from .vault import get_vault

VAULT_PATH = 'playbooks/vars/meltano/admins.yml.vlt'

def get_admins(hostname):
  vault = get_vault()
  admins = vault.load(open(VAULT_PATH, 'r').read())

  try:
    return admins[hostname]
  except KeyError:
    return None