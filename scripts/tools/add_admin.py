import string
import random
from .vault import get_vault

DOMAIN_NAME = 'meltanodata.com'
VAULT_PATH = 'playbooks/vars/meltano/admins.yml.vlt'

def add_admin(subdomain, username, password = None):
  hostname = f"{subdomain}.{DOMAIN_NAME}"

  if not password:
    password = ''.join(random.sample('0123456789' + string.ascii_letters, 20))

  vault = get_vault()
  admins = vault.load(open(VAULT_PATH, 'r').read())

  if hostname not in admins:
    admins[hostname] = []

  existing_admin = next((a for a in admins[hostname] if a['username'] == username), None)
  if existing_admin:
    print(f"Admin for https://{hostname} already exists:")
    print()
    print(f"Username: {existing_admin['username']}")
    print(f"Password: {existing_admin['password']}")

    return (existing_admin, False)

  new_admin = {
    "username": username,
    "password": password
  }
  admins[hostname].append(new_admin)

  vault.dump(admins, open(VAULT_PATH, 'wb'))

  print(f"Added admin user for https://{hostname}:")
  print()
  print(f"Username: {new_admin['username']}")
  print(f"Password: {new_admin['password']}")

  return (new_admin, True)