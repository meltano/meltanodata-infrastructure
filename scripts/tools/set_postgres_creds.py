from urllib.parse import urlparse
from .vault import get_vault

DOMAIN_NAME = 'meltanodata.com'
VAULT_PATH = 'playbooks/vars/meltano/postgres_creds.yml.vlt'

def set_postgres_creds(subdomain, uri):
  hostname = f"{subdomain}.{DOMAIN_NAME}"
  parsed_uri = urlparse(uri)

  vault = get_vault()
  postgres_creds = vault.load(open(VAULT_PATH, 'r').read())

  creds = {
    "username": parsed_uri.username,
    "password": parsed_uri.password,
    "address": parsed_uri.hostname,
    "port": parsed_uri.port,
    "database": parsed_uri.path[1:],
  }

  if hostname in postgres_creds and postgres_creds[hostname] == creds:
    print(f"Postgres connection details for https://{hostname} have already been set")

    return (postgres_creds[hostname], False)

  postgres_creds[hostname] = creds

  vault.dump(postgres_creds, open(VAULT_PATH, 'wb'))

  print(f"Set Postgres connection details for https://{hostname}:")
  print()
  print(f"PG_USERNAME={creds['username']}")
  print(f"PG_PASSWORD={creds['password']}")
  print(f"PG_ADDRESS={creds['address']}")
  print(f"PG_PORT={creds['port']}")
  print(f"PG_DATABASE={creds['database']}")

  return (creds, True)
