import sys
import requests
import logging
import json
import re

DOMAIN_NAME = 'meltanodata.com'

# Mostly copied over from ../inventory/digital_ocean.py
class DoManager:
    def __init__(self, api_token):
        self.api_token = api_token
        self.api_endpoint = 'https://api.digitalocean.com/v2'
        self.headers = {'Authorization': 'Bearer {0}'.format(self.api_token),
                        'Content-type': 'application/json'}
        self.timeout = 60
        self.cache = {}

    def _url_builder(self, path):
        if path[0] == '/':
            path = path[1:]
        return '%s/%s' % (self.api_endpoint, path)

    def send_get(self, url):
        url = self._url_builder(url)
        try:
            resp_data = {}
            incomplete = True
            while incomplete:
                resp = requests.get(url, headers=self.headers, timeout=self.timeout)

                if resp.status_code != 200:
                    # Retry
                    continue

                json_resp = resp.json()

                for key, value in json_resp.items():
                    if isinstance(value, list) and key in resp_data:
                        resp_data[key] += value
                    else:
                        resp_data[key] = value

                try:
                    url = json_resp['links']['pages']['next']
                except KeyError:
                    incomplete = False

        except ValueError as e:
            sys.exit("Unable to parse result from %s: %s" % (url, e))
        return resp_data

    def send_delete(self, url):
        url = self._url_builder(url)
        resp = requests.delete(url, headers=self.headers, timeout=self.timeout)
        return resp.status_code == 204

    def all_droplets(self):
        if 'droplets' not in self.cache:
            resp = self.send_get('droplets?tag_name=production')
            self.cache['droplets'] = resp['droplets']

        return self.cache['droplets']


    def all_databases(self):
        if 'databases' not in self.cache:
            resp = self.send_get('databases?tag_name=production')
            self.cache['databases'] = resp['databases']

        return self.cache['databases']

    def all_domain_records(self):
        if 'domain_records' not in self.cache:
            resp = self.send_get('domains/%s/records' % DOMAIN_NAME)
            self.cache['domain_records'] = resp['domain_records']

        return self.cache['domain_records']

    def delete_droplet(self, droplet_id):
        return self.send_delete('droplets/%s' % droplet_id)

    def delete_database(self, database_id):
        return self.send_delete('databases/%s' % database_id)

    def delete_domain_record(self, record_id):
        return self.send_delete('domains/%s/records/%s' % (DOMAIN_NAME, record_id))

class InstanceDeleterError(Exception):
    pass

class InstanceDeleter:
    def __init__(self, api_token, database_api_tokens=list()):
        self.manager = DoManager(api_token)
        self.database_managers = [
            self.manager,
            *(DoManager(api_token) for api_token in database_api_tokens)
        ]

    def delete_instance(self, subdomain):
        self.delete_droplet(subdomain)
        print()

        self.delete_domain_record(subdomain)
        print()

        self.delete_database(subdomain)

    def delete_droplet(self, subdomain):
        print(f"Finding droplet for subdomain '{subdomain}'...")

        all_droplets = self.manager.all_droplets()
        droplet = next(
            (
                droplet
                for droplet in all_droplets
                if droplet["name"] == f"{subdomain}.{DOMAIN_NAME}"
            ),
            None
        )

        if droplet is None:
            print(f"Droplet for subdomain '{subdomain}' could not be found. Skipping")
            return

        droplet_id = droplet["id"]
        print(f"Found droplet with ID '{droplet_id}' for subdomain '{subdomain}'!")

        print(f"Deleting droplet...")

        success = self.manager.delete_droplet(droplet_id)
        if not success:
            raise InstanceDeleterError(f"Droplet with ID '{droplet_id}' for subdomain '{subdomain}' could not be deleted")

        print("Successfully deleted droplet!")

    def delete_domain_record(self, subdomain):
        print(f"Finding domain record for subdomain '{subdomain}'...")

        all_records = self.manager.all_domain_records()
        record = next(
            (
                record
                for record in all_records
                if record["type"] == "A"
                and record["name"] == subdomain
            ),
            None
        )

        if record is None:
            print(f"Record for subdomain '{subdomain}' could not be found. Skipping")
            return

        record_id = record["id"]
        print(f"Found record with ID '{record_id}' for subdomain '{subdomain}'!")

        print(f"Deleting record...")

        success = self.manager.delete_domain_record(record_id)
        if not success:
            raise InstanceDeleterError(f"Record with ID '{record_id}' for subdomain '{subdomain}' could not be deleted")

        print("Successfully deleted record!")

    def delete_database(self, subdomain):
        print(f"Finding database for subdomain '{subdomain}'...")

        for index, manager in enumerate(self.database_managers, start=1):
            print(f"Trying DigitalOcean account {index}...")

            all_databases = manager.all_databases()
            database = next(
                (
                    database
                    for database in all_databases
                    if re.search(rf"[0-9]+-{subdomain}$", database["name"])
                ),
                None
            )
            if database:
                break

        if database is None:
            print(f"Database for subdomain '{subdomain}' could not be found. Skipping")
            return

        database_id = database["id"]
        print(f"Found database with ID '{database_id}' for subdomain '{subdomain}'!")

        print(f"Deleting database...")

        success = manager.delete_database(database_id)
        if not success:
            raise InstanceDeleterError(f"Database with ID '{database_id}' for subdomain '{subdomain}' could not be deleted")

        print("Successfully deleted database!")