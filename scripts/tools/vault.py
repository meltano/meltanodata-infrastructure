from ansible_vault import Vault

def get_vault():
  vault_password = open('.vault-password').read().rstrip()
  return Vault(vault_password)
