from urllib.parse import urlparse
from .vault import get_vault

DOMAIN_NAME = 'meltanodata.com'
VAULT_PATH = 'playbooks/vars/meltano/postgres_creds.yml.vlt'

def delete_postgres_creds(subdomain):
  hostname = f"{subdomain}.{DOMAIN_NAME}"

  vault = get_vault()
  postgres_creds = vault.load(open(VAULT_PATH, 'r').read())

  try:
    del postgres_creds[hostname]
  except KeyError:
    print(f"No Postgres connection details found to delete for https://{hostname}")

    return False

  vault.dump(postgres_creds, open(VAULT_PATH, 'wb'))

  print(f"Deleted Postgres connection details for https://{hostname}")

  return True
