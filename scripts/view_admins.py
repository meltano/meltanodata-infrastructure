#!/usr/bin/env python3

"""
Script to view Meltano UI admin records stored in the playbooks/vars/meltano/admins.yml.vlt vault.

To be run from the infrastructure project root.

Usage:
    ./scripts/view_admins.py subdomain
"""

import sys
from tools import get_admins

DOMAIN_NAME = 'meltanodata.com'

def main():
  subdomain = sys.argv[1]
  hostname = f"{subdomain}.{DOMAIN_NAME}"

  print(f"Admin users for https://{hostname}:")
  print()

  admins = get_admins(hostname)

  if admins:
    for admin in admins:
      print(f"Username: {admin['username']}")
      print(f"Password: {admin['password']}")
      print()
  else:
    print("No admins found")

if __name__ == '__main__':
    main()