#!/usr/bin/env python3

"""
Script to view Postgres connection details stored in the playbooks/vars/meltano/postgres_creds.yml.vlt vault.

To be run from the infrastructure project root.

Usage:
    ./scripts/view_postgres_creds.py subdomain
"""

import sys
from tools import get_postgres_creds

DOMAIN_NAME = 'meltanodata.com'

def main():
  subdomain = sys.argv[1]
  hostname = f"{subdomain}.{DOMAIN_NAME}"

  print(f"Postgres connection details for https://{hostname}:")
  print()

  creds = get_postgres_creds(hostname)

  if creds:
    print(f"PG_USERNAME={creds['username']}")
    print(f"PG_PASSWORD={creds['password']}")
    print(f"PG_ADDRESS={creds['address']}")
    print(f"PG_PORT={creds['port']}")
    print(f"PG_DATABASE={creds['database']}")
  else:
    print("No connection details found")

if __name__ == '__main__':
    main()