#!/usr/bin/env python3

"""
Script to bring down a MeltanoData instance.

Environment variables:
    DO_API_TOKEN: API token for the main 'Meltano' DO account
    DO_DATABASE_API_TOKENS: Comma-separated API tokens for the 'MeltanoData DBs' DO accounts

Usage:
    ./bringdown.py subdomain
"""

import os
import sys
import subprocess
import tools
from tools.instance_deleter import InstanceDeleter

def run_cmd(*args):
  print()
  print(f"--- {str(args)[1:-1]} ---")
  res = subprocess.run(args, check=True)
  print(f"---")

  return res

def run_func(func, *args):
  print()
  print(f"--- {func.__name__}({str(args)[1:-1]}) ---")
  res = func(*args)
  print(f"---")

  return res
  

DOMAIN_NAME = 'meltanodata.com'

if "DO_API_TOKEN" in os.environ:
    api_token = os.getenv("DO_API_TOKEN")
else:
    raise Exception("API token for the main 'Meltano' DO account must be provided in DO_API_TOKEN environment variable")

if "DO_DATABASE_API_TOKENS" in os.environ:
    tokens_string = os.getenv("DO_DATABASE_API_TOKENS")
    database_api_tokens = [c.strip() for c in tokens_string.split(",")]
else:
    raise Exception("Comma-separated API tokens for the 'MeltanoData DBs' DO accounts must be provided in DO_API_TOKEN environment variable")

try:
    subdomain = sys.argv[1]
except IndexError:
    raise Exception("Subdomain must be provided in first command line argument")

hostname = f"{subdomain}.{DOMAIN_NAME}"

print(f"Bringing down instance {hostname}...")

run_cmd("pip3", "install", "-r", "requirements.txt")

run_cmd("dist/pull.sh")

deleted = run_func(tools.delete_admins, subdomain)
if deleted:
  run_cmd("git", "commit", "-am", "Delete admin users for old instance")
  run_cmd("git", "push")

deleted = run_func(tools.delete_postgres_creds, subdomain)

if deleted:
  run_cmd("git", "commit", "-am", "Delete Postgres connection details for old instance")
  run_cmd("git", "push")

deleter = InstanceDeleter(api_token, database_api_tokens)
run_func(deleter.delete_instance, subdomain)

print()
print(f"Successfully brought down instance {hostname}!")