#!/bin/env python3
#
# This script can be used to extract authentication information
# from 1Password using the `op` command client.
#

import subprocess
import json
import yaml


def op(*args) -> dict:
  result = subprocess.run(
    ["/usr/bin/op", *args],
    stdout=subprocess.PIPE
  )

  return json.loads(result.stdout)


def item_field(item, name: str) -> dict:
  return next(
      field
      for field in item["details"]["fields"]
      if field["designation"] == name
  )


def item_entry(item) -> dict:
  item = op("get", "item", item["uuid"])

  return {
    item["overview"]["title"]: dict(
      username = item_field(item, "username")["value"],
      password = item_field(item, "password")["value"]
    )
  }


vault_items = op("list", "items", "--vault=meltanodata.com")
entries = list(map(item_entry, vault_items))

print(yaml.dump(entries))
