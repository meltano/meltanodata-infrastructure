#!/usr/bin/env python3

"""
Script to delete Meltano UI admin records from the playbooks/vars/meltano/admins.yml.vlt vault.

To be run from the infrastructure project root.

Usage:
    ./scripts/delete_admins.py subdomain
"""

import sys
from tools import delete_admins

def main():
  subdomain = sys.argv[1]

  delete_admins(subdomain)

if __name__ == '__main__':
    main()