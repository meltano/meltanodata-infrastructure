#!/usr/bin/env python3

"""
Script to delete MeltanoData.com instances (droplet, domain record, and database) from DigitalOcean.

Environment variables:
    DO_API_TOKEN: API token for the main 'Meltano' DO account
    DO_DATABASE_API_TOKENS: Comma-separated API tokens for the 'MeltanoData DBs' DO accounts

Usage:
    ./delete_instances.py [subdomain ...]

Example:
    ./delete_instances.py user1 user2
"""

import os
import sys
from tools.instance_deleter import InstanceDeleter

def main():
    if "DO_API_TOKEN" in os.environ:
        api_token = os.getenv("DO_API_TOKEN")
    else:
        msg = "API token for the main 'Meltano' DO account must be provided in DO_API_TOKEN environment variable\n"
        sys.stderr.write(msg)
        sys.exit(-1)

    if "DO_DATABASE_API_TOKENS" in os.environ:
        tokens_string = os.getenv("DO_DATABASE_API_TOKENS")
        database_api_tokens = [c.strip() for c in tokens_string.split(",")]
    else:
        msg = "API tokens for the 'MeltanoData DBs' DO accounts must be provided in comma-separated DO_DATABASE_API_TOKENS environment variable\n"
        sys.stderr.write(msg)
        sys.exit(-1)

    if len(sys.argv) == 1:
        msg = 'MeltanoData.com subdomain must be provided in command line arguments\n'
        sys.stderr.write(msg)
        sys.exit(-1)

    deleter = InstanceDeleter(api_token, database_api_tokens)

    for subdomain in sys.argv[1:]:
        deleter.delete_instance(subdomain)
        print()

if __name__ == '__main__':
    main()