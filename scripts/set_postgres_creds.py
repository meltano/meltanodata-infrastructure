#!/usr/bin/env python3

"""
Script to set Postgres connection details in the playbooks/vars/meltano/postgres_creds.yml.vlt vault.

To be run from the infrastructure project root.

Usage:
    ./scripts/set_postgres_creds.py subdomain postgres://user:password@host:port/database
"""

import sys
from tools import set_postgres_creds

def main():
  subdomain = sys.argv[1]
  uri = sys.argv[2]

  set_postgres_creds(subdomain, uri)

if __name__ == '__main__':
    main()